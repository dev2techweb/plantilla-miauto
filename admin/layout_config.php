<?php

function motors_layout_plugins($layout, $get_layouts = false)
{
    $required = array(
        'stm-post-type',
        'stm-motors-extends',
        'custom_icons_by_stylemixthemes',
        'stm_importer',
        'js_composer',
        'revslider',
        'breadcrumb-navxt',
        'contact-form-7',
        'mailchimp-for-wp',
    );

    $plugins = array(
        'car_magazine' => array(
            'stm_vehicles_listing',
            'stm-megamenu',
            'instagram-feed',
            'accesspress-social-counter',
            'stm_motors_events',
			'add-to-any',
            'stm_motors_review'
        ),
        'service' => array(
            'stm_vehicles_listing',
            'instagram-feed',
            'bookly-responsive-appointment-booking-tool',
			'add-to-any',
        ),
        'listing' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
            'instagram-feed',
            'subscriptio',
            'wordpress-social-login',
			'add-to-any',
            'woocommerce'
        ),
        'listing_two' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
            'instagram-feed',
            'subscriptio',
            'wordpress-social-login',
            'woocommerce',
			'add-to-any',
            'stm_motors_review'
        ),
        'listing_three' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
            'instagram-feed',
            'subscriptio',
            'wordpress-social-login',
            'woocommerce',
			'add-to-any',
            'stm_motors_review'
        ),
        'listing_four' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
            'instagram-feed',
            'subscriptio',
            'wordpress-social-login',
			'add-to-any',
            'woocommerce'
        ),
        'car_dealer' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
            'instagram-feed',
			'add-to-any',
            'woocommerce',
        ),
        'car_dealer_two' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
            'instagram-feed',
			'add-to-any',
            'woocommerce',
        ),
        'motorcycle' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
            'instagram-feed',
            'woocommerce',
			'add-to-any',
        ),
        'boats' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
            'instagram-feed',
            'woocommerce',
			'add-to-any',
        ),
        'car_rental' => array(
            'stm_vehicles_listing',
            'instagram-feed',
            'woocommerce',
			'add-to-any',
        ),
        'auto_parts' => array(
            'stm-woocommerce-motors-auto-parts',
            'pearl-header-builder',
            'woo-multi-currency',
            'yith-woocommerce-compare',
            'yith-woocommerce-wishlist',
            'woocommerce',
			'add-to-any',
        ),
        'aircrafts' => array(
            'stm_vehicles_listing',
			'stm-megamenu',
			'add-to-any',
            'woocommerce'
        ),
        'rental_two' => array(
            'stm-motors-car-rental',
            'woocommerce'
        ),
		'equipment' => array(
			'stm_vehicles_listing',
			'stm-motors-equipment'
		)
    );

    if ($get_layouts) return $plugins;

    return array_merge($required, $plugins[$layout]);
}